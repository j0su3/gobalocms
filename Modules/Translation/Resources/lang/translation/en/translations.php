<?php

return [
    'title' => [
        'translations' => 'Translations',
        'edit translation' => 'Edit a translation',
    ],
    'button' => [
        'create translation' => 'Create a translation',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'Clear translation cache' => 'Clear translations cache',
    'Export' => 'Export',
    'Import' => 'Import',
    'Import csv translations file' => 'Import csv translations file',
    'only for developers' => 'Will require git management!',
    'Translations exported' => 'Translations exported.',
    'Translations imported' => 'Translations imported.',
    'select CSV file' => 'Select CSV file ...',
    'csv only allowed' => 'Please upload a CSV file only.',
    'time' => 'Time',
    'author' => 'Author',
    'history' => 'History',
    'No Revisions yet' => 'No Revisions yet',
    'event' => 'Event',
    'created' => 'Created',
    'edited' => 'Edited',
    'revert history' => 'Revert History',
    'list resource' => 'List translations',
    'edit resource' => 'Edit translations',
    'import resource' => 'Import translations',
    'export resource' => 'Export translations',
    /* GOBALO MOD */
    'Available translations' => 'Available translations',
    'Inactive translation' => 'Inactive translation',
    'Language copied successfully' => 'Language copied successfully',
    'Languages copied successfully' => 'Languages copied successfully',
    'copy' => 'copy',
    'Check all' => 'Check all',
    'Attention' => 'Attention',
    'The changes will take effect immediately' => 'The changes will take effect immediately',
    
    // Languages
    'en' => 'English',
    'es' => 'Spanish'
    /* FING GOBALO MOD */
];
