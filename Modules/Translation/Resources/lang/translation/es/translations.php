<?php

return [
    'title' => [
        'translations' => 'Traducciones',
        'edit translation' => 'Editar una traducción',
    ],
    'button' => [
        'create translation' => 'Crear una traducción',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'Clear translation cache' => 'Limpiar cache de traducciones',
    'Export' => 'Exportar',
    'Import' => 'Importar',
    'Import csv translations file' => 'Importar archivo CSV de traducciones',
    'only for developers' => 'Will require git management!',
    'Translations exported' => 'Traducciones exportadas.',
    'Translations imported' => 'Traducciones importadas.',
    'select CSV file' => 'Seleccionar el archivo CSV...',
    'csv only allowed' => 'Por favor, solo suba un archivo CSV.',
    'time' => 'Hora',
    'author' => 'Autor',
    'history' => 'Historial',
    'No Revisions yet' => 'No hay revisiones',
    'event' => 'Evento',
    'created' => 'Creado',
    'edited' => 'Editado',
    'revert history' => 'Revertir Historial',
    'list resource' => 'Listar traducciones',
    'edit resource' => 'Editar traducciones',
    'import resource' => 'Importar traducciones',
    'export resource' => 'Exportar traducciones',
    /* GOBALO MOD */
    'Available translations' => 'Traducciones disponibles',
    'Inactive translation' => 'Traducción inactiva',
    'Language copied successfully' => 'Idioma copiado con éxito',
    'Languages copied successfully' => 'Idiomas copiados con éxito',
    'copy' => 'copia',
    'Check all' => 'Marcar todas',
    'Attention' => 'Atención',
    'The changes will take effect immediately' => 'Los cambios se realizarán de manera inmediata',
    
    // Languages
    'en' => 'Inglés',
    'es' => 'Español'
    
    /* FIN GOBALO MOD */
];

