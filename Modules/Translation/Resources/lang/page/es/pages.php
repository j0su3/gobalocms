<?php

return [
    'pages' => 'Páginas',
    'create page' => 'Crear una página',
    'edit page' => 'Editar una página',
    'name' => 'Nombre',

    'slug' => 'URL amigable',
    'title' => 'Título',

    'meta_data' => 'Datos de Meta',
    'meta_title' => 'Título para el Meta',
    'meta_description' => 'Descripción para el Meta',
    'facebook_data' => 'Datos de Facebook',
    'og_title' => 'Título para Facebook',
    'og_description' => 'Descripción para Facebook',
    'og_type' => 'Tipo Facebook',
    /* GOBALO MOD */
    'attributes' => 'Atributos',
    'parent page' => 'Página superior',
    'publish' => 'Publicar',
    'publish date' => 'Fecha de publicación',
    /* FIN GOBALO MOD */
    'template' => 'Nombre de la plantilla de la página',
    'is homepage' => '¿Es la Página principal?',
    /* GOBALO MOD */
    'homepage' => 'Página principal',
    /* FIN GOBALO MOD */
    'body' => 'Cuerpo',
    'status' => 'Estado',
    /* GOBALO MOD */
    'statuses' => [
        'published' => 'Publicada',
        'scheduled' => 'Programada',
        'draft' => 'Borrador'
     ],
    'visibility' => 'Visibilidad',
    'visibilities' => [
        'public' => 'Pública',
        'private' => 'Privada'
    ],
    'author' => 'Autor',
    /* FIN GOBALO MOD */
    'pages were updated' => 'Las Páginas fueron actualizadas',

    'back to index' => 'Volver a la lista de páginas',
    'list resource' => 'Lista páginas',
    'create resource' => 'Crear páginas',
    'edit resource' => 'Editar páginas',
    'destroy resource' => 'Eliminar páginas',
    /* GOBALO MOD */
    'select parent page' => 'Selecciona página superior',
    'parent page seleted' => 'Página superior seleccionada',
    /* FIN GOBALO MOD */
    'view-page' => 'Ver  página',
    'edit-page' => 'Editar página',
    'validation' => [
        'attributes' => [
            'title' => 'Título',
            'body' => 'Cuerpo',
        ],
    ],
    'facebook-types' => [
        'website' => 'Página web',
        'product' => 'Producto',
        'article' => 'Artículo',
    ],

    //

    'activate parent translation' => 'Activa la traducción de la página superior',
    'activate parent and deactivate children translations' => 'Activa la traducción de la página superior y desactiva las traducciones de las páginas inferiores',
    'deactivate children translations' => 'Desactiva las traducciones de las páginas inferiores',

    //

    'parent page assigned' => 'Esta página tiene asignada la siguiente página superior',
    'children pages assigned' => 'Esta acción provocará que las siguientes páginas se conviertan en páginas superiores'

];
