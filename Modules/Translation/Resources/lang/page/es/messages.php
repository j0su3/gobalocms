<?php

return [
    'page created' => 'Página creada exitosamente.',
    'page not found' => 'Página no encontrada.',
    'page updated' => 'Página actualizada exitosamente.',
    'page deleted' => 'Página eliminada exitosamente.',
    'page deleted trash' => 'Página enviada a la papelera exitosamente.',

    'template is required' => 'El nombre de la plantilla es requerido.',
    'title is required' => 'El título es requerido.',
    'slug is required' => 'La URL amigable es requerida.',
    /* GOBALO MOD */
    'slug regex' => 'Sólo se permiten caracteres del alfabeto inglés, números, guión y guión bajo.',
    'slug is unique' => 'La URL amigable ya existe.',
    'slug is reserved' => 'Esta URL amigable está reservada por el sistema.',
    /* FIN GOBALO MOD */
    'body is required' => 'El cuerpo es requerido.',
    'only one homepage allowed' => 'Sólo es permitida una página principal.',
];
