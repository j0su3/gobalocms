<?php

return [
    'pages' => 'Pages',
    'create page' => 'Create a page',
    'edit page' => 'Edit a page',
    'name' => 'Name',

    'slug' => 'Slug',
    'title' => 'Title',
    
    'meta_data' => 'Meta data',
    'meta_title' => 'Meta title',
    'meta_description' => 'Meta description',
    'facebook_data' => 'Facebook data',
    'og_title' => 'Facebook title',
    'og_description' => 'Facebook description',
    'og_type' => 'Facebook type',
     /* GOBALO MOD */
    'attributes' => 'Attributes',
    'parent page' => 'Top page',
    'publish' => 'Publish',
    'publish date' => 'Publish date',
    /* FIN GOBALO MOD */
    'template' => 'Page template name',
    'is homepage' => 'Homepage ?',
    /* GOBALO MOD */
    'homepage' => 'Homepage',
    /* FIN GOBALO MOD */
    'body' => 'Body',
    'status' => 'Status',
    /* GOBALO MOD */
    'statuses' => [
        'published' => 'Published',
        'scheduled' => 'Scheduled',
        'draft' => 'Draft'
    ],
    'visibility' => 'Visibility',
    'visibilities' => [
        'public' => 'Public',
        'private' => 'Private'
    ],
    'author' => 'Author',
    /* FIN GOBALO MOD */
    'pages were updated' => 'Pages were updated',

    'back to index' => 'Go back to the pages index',
    'list resource' => 'List pages',
    'create resource' => 'Create pages',
    'edit resource' => 'Edit pages',
    'destroy resource' => 'Delete pages',
    /* GOBALO MOD */
    'select parent page' => 'Select top page',
    'parent page seleted' => 'Top page seleted',
    /* FIN GOBALO MOD */
    'view-page' => 'View page',
    'edit-page' => 'Edit page',
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Website',
        'product' => 'Product',
        'article' => 'Article',
    ],

    //

    'activate parent translation' => 'Activate the translation of the top page',
    'activate parent and deactivate children translations' => 'Activate the translation of the top page and deactivate the bottom pages translations',
    'deactivate children translations' => 'Deactivate the bottom pages translations',

    //

    'parent page assigned' => 'This page is assigned the following top page',
    'children pages assigned' => 'This action will cause the following pages to become top pages'

];
