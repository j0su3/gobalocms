<?php

return [
    'page created' => 'Page successfully created.',
    'page not found' => 'Page not found.',
    'page updated' => 'Page successfully updated.',
    'page deleted' => 'Page successfully deleted.',
    'page deleted trash' => 'Page successfully moved to trash.',

    'template is required' => 'The template name is required.',
    'title is required' => 'The title is required.',
    'slug is required' => 'The slug is required.',
    /* GOBALO MOD */
    'slug regex' => 'Only English alphabet characters, numbers, hyphen or underscore are allowed.',
    'slug is unique' => 'The slug has already been taken.',
    'slug is reserved' => 'This slug is reserved by the system.',
    /* FIN GOBALO MOD */
    'body is required' => 'The body is required.',
    'only one homepage allowed' => 'Only one homepage is allowed',
];
