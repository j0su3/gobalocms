<?php

return [
    'posts' => 'Posts',
    
    //
    
    'create post' => 'Create post',
    'edit post' => 'Edit post',
    
    //
    
    'title' => 'Title',
    'slug' => 'Slug',
    'content' => 'Content',
    'category' => 'Category',
    'categories' => 'Categories',
    'author' => 'Author',
    'status' => 'Status',
    'visibility' => 'Visibility',
    'publish date' => 'Publish date',
    'publish' => 'Publish',
    
    'main category' => 'Main category',
    
    //
    
    'visibilities' => [
        'public' => 'Public',
        'private' => 'Private'
    ],
    
    'statuses' => [
        'published' => 'Published',
        'scheduled' => 'Scheduled',
        'draft' => 'Draft'
    ],
    
    'attributes' => 'Attributes',
    'select category' => 'Select category',
    'to select main category click it' => 'To select main category click it',

    //

    'activate translations associated categories' => 'Activate this translation of the associated categories'

];