<?php

return [
    'categories' => 'Categories',
    
    //
    
    'create category' => 'Create category',
    'edit category' => 'Edit category',
    
    //
    
    'name' => 'Name',
    'slug' => 'Slug',
    'description' => 'Description',
    'parent category' => 'Top category',
    'posts' => 'Posts',

    //
    
    'attributes' => 'Attributes',
    'select parent category' => 'Select top category',

    //

    'parent category assigned' => 'This category is assigned the following parent category',
    'children categories assigned' => 'This action will cause the following categories to become higher categories',

    //

    'activate parent translation' => 'Activate the translation of the top category',
    'activate parent and deactivate children translations' => 'Activate the translation of the top category and deactivate the bottom categories translations',
    'deactivate children translations' => 'Deactivate the bottom categories translations',

    'deactivate children translations and posts translations' => 'Deactivate the bottom categories translations and posts assigned translations',
    'activate parent translation and deactivate post translations' => 'Activate the translation of the top category and deactivate the posts assigned translations',
    'activate parent translation ans posts' => 'Activate the translation of the top category and posts assigned translations',
    'deactivate posts assigned' => 'Deactivate posts assigned translations',

    //

    'category page assigned' => 'This page is assigned the following top category',
    'children categories assigned' => 'This action will cause the following categories to become top categories'

];