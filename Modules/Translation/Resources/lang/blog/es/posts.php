<?php

return [
    'posts' => 'Entradas',
    
    //
    
    'create post' => 'Crear una entrada',
    'edit post' => 'Editar una entrada',
    
    //
    
    'title' => 'Título',
    'slug' => 'URL amigable',
    'content' => 'Contenido',
    'category' => 'Categoría',
    'categories' => 'Categorías',
    'author' => 'Autor',
    'status' => 'Estado',
    'visibility' => 'Visibilidad',
    'publish date' => 'Fecha de publicación',
    'publish' => 'Publicar',
    
    'main category' => 'Categoría principal',
    
    //
    
    'visibilities' => [
        'public' => 'Público',
        'private' => 'Privado'
    ],
    
    'statuses' => [
        'published' => 'Publicada',
        'scheduled' => 'Programada',
        'draft' => 'Borrador'
    ],
    
    'attributes' => 'Atributos',
    'select category' => 'Seleccione categoría',
    'to select main category click it' => 'Para seleccionar la categoría principal haz click sobre ella',

    //

    'activate translations associated categories' => 'Active esta traducción en las categorías asociadas'

];