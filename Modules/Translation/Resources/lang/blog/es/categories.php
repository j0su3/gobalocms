<?php

return [
    'categories' => 'Categorías',
    
    //
    
    'create category' => 'Crear una categoría',
    'edit category' => 'Editar una categoría',
    
    //
    
    'name' => 'Nombre',
    'slug' => 'URL amigable',
    'description' => 'Descripción',
    'parent category' => 'Categoría superior',
    'posts' => 'Entradas',

    //
    
    'attributes' => 'Atributos',
    'select parent category' => 'Selecciona categoría superior',

    //

    'parent category assigned' => 'Esta categoría tiene asignada la siguiente categoría padre',
    'children categories assigned' => 'Esta acción provocará que las siguientes categorías se conviertan en categorías superiores',

    //

    'activate parent translation' => 'Activa la traducción de la categoría superior',
    'activate parent and deactivate children translations' => 'Activa la traducción de la categoría superior y desactiva las traducciones de las categorías inferiores',
    'deactivate children translations' => 'Desactiva las traducciones de las categorías inferiores',

    'deactivate children translations and posts translations' => 'Desactive las traducciones de las categoría inferiores y las traducciones de los posts asociados',
    'activate parent translation and deactivate post translations' => 'Active la traducción de la categoría superior y desactive las traducciones de los post asociados',
    'activate parent translation ans posts' => 'Active la traducción de la categoría superior y las traducciones de los posts asociados',
    'deactivate posts assigned' => 'Desactive las traducciones de los posts asociados',

    //

    'category page assigned' => 'Esta categoría tiene asignada la siguiente categoría superior',
    'children categories assigned' => 'Esta acción provocará que las siguientes catgeorías se conviertan en categorías superiores'

];