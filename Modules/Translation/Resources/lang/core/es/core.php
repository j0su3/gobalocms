<?php

return [
    'modal' => [
        'title' => 'Confirmación',
        /* GOBALO MOD */
        'title-attention' => '¡Atención!',
        
        'confirmation-message' => '¿Estás seguro que quieres eliminar este elemento?',
        'confirmation-delete-bulk-message' => 'Vas a eliminar todos los elementos seleccionados. ¿Estás seguro?',
        'confirmation-children-items' => 'Eliminar este elemento implica que todos sus hijos directos serán desacoplados y se convertirán en elementos padre',
        'confirmation-children-show' => 'Los siguientes elementos se convertirán en elementos padre',
        'confirmation-children-confirmation' => '¿Estas seguro que deseas eliminar estos elementos?',
        
        // Eliminar
        'confirmation-message-info-parent' => "Si eliminas el padre de este elemento, esté dejará de ser hijo del mismo. ¿Estás seguro?",
        
        // Papelera
        'confirmation-message-info-parent-trash' => 'Al eliminar este elemento, éste dejará de ser hijo de',
        'confirmation-message-trash' => '¿Estás seguro que quieres mover este elemento a la papelera?',
        'confirmation-delete-bulk-message-trash' => 'Vas a mover todos los elementos seleccionados a la papelera. ¡Recuerda! Si alguno de estos elementos tiene hijos directos, estos se convertirán en elementos principales. ¿Estás seguro?',
        'confirmation-children-items-trash' => 'Mover este elemento a la papelera implica que todos sus hijos directos serán desacoplados y se convertirán en elementos padre',
        'confirmation-children-show-trash' => 'Los siguientes elementos se convertirán en elementos padre',
        'confirmation-children-confirmation-trash' => '¿Estas seguro que deseas mover estos elementos a la papelera?',
        /* FIN GOBALO MOD */
    ],
    'table' => [
        'created at' => 'Creado el',
        'updated at' => 'Actualizado el',
        'deleted at' => 'Eliminado el',
        'actions' => 'Acciones',
        'thumbnail' => 'Miniaturas',
    ],
    'tab' => [
        'english' => 'Inglés',
        'french' => 'Frances',
        'dutch' => 'Holandes',
        'italian' => 'Italiano',
        'greek' => 'Griego',
        'spanish' => 'Español',
        'polish' => 'Polaco',
        'portuguese' => 'Portugués',
        'arabic' => 'Árabe',
        'macedonian' => 'Macedónio',
        'turkish' => 'Turco',
        'czech' => 'Checo',
        'swedish' => 'Sueco',
        'korean' => 'Korean',
    ],
    'button' => [
        'cancel' => 'Cancelar',
        'create' => 'Crear',
        'update' => 'Actualizar',
        'delete' => 'Eliminar',
        'reset' => 'Reiniciar',
        'see' => 'Ver',
        'visualize' => 'Visualize',
        'update and back' => 'Actualizar y regresar',
        /* GOBALO MOD */
        'move-to-trash' => 'Mover a la papelera',
        'back' => 'Volver'
        /* FIN GOBALO MOD */
    ],
    'save' => 'Guardar',
    'confirm' => 'Confirmar',
    'move' => 'Mover',
    'widget' => 'Complemento',
    'widgets' => 'Complementos',
    'breadcrumb' => [
        'home' => 'Inicio',
    ],
    'title' => [
        'translatable fields' => 'Campos a traducir',
        'non translatable fields' => 'Campos no traducibles',
        'create resource' => 'Crear :name',
        'edit resource' => 'Editar :name',
    ],
    'general' => [
        'available keyboard shortcuts' => 'Atajos de teclado disponibles en esta página',
        'view website' => 'Ver sitio',
        'complete your profile' => 'Complete su perfil',
        'profile' => 'Perfil',
        'sign out' => 'Salir',
    ],
    'messages' => [
        'resource created' => ':name creado éxitosamente.',
        'resource not found' => ':name no encontrado.',
        'resource updated' => ':name actualizado éxitosamente.',
        'resource deleted' => ':name eliminado éxitosamente.',
    ],
    'back' => 'Atrás',
    'back to index' => 'Regresar a :name',
    'permission denied' => 'Permiso denegado. (permiso requerido: ":permission")',
    'list resource' => 'Lista :name',
    'create resource' => 'Crear :name',
    'edit resource' => 'Editar :name',
    'destroy resource' => 'Eliminar :name',
    'error token mismatch' => 'Tu session se ha terminado, por favor envia el formulario nuevamente.',
    'error 404' => '404',
    'error 404 title' => 'Oops! Lo sentimos esta página no fue encontrada.',
    'error 404 description' => 'La página que estas buscando no fue encontrada.',
    'error 500' => '500',
    'error 500 title' => 'Oops! Algo salió mal',
    'error 500 description' => 'El Administrador ha sido notificado.',
    'delete cancelled' => 'Eliminar cancelado.',
    'unauthorized' => 'No autorizado',
    'unauthorized-access' => 'No tienes los permisos apropiados para acceder a esa página.',
    'unauthenticated' => 'No autenticado',
    'unauthenticated-access' => 'Debes iniciar sesión para acceder a esta página',
    'something went wrong' => 'Whoops!  Algo salió mal.',
    'mark as online' => 'Marcar como Activo',
    'mark as offline' => 'Marcar como Inactivo',
    'back to backend' => 'Regresar a la Administración',
    /* GOBALO MOD */
    /* GOBALO MOD */
    'Copy languages?' => 'Todas las traducciones disponibles serán sobreescritas. ¿Estás seguro que deseas realizar esta acción?',
    'Delete items selected' => 'Eliminar elementos seleccionados',
    'Deleting...' => 'Eliminando...',
    'Items deleted successfully' => 'Los elementos seleccionados fueron eliminados con éxito',
    'Items deleted successfully trash' => 'Los elementos seleccionados se enviaron a la papelera',
    'restore' => 'Recuperar',
    'trash' => 'Papelera',
    'list' => 'Listado',
    'Generate' => 'Generar',
    //
    'Error' => 'Error',
    'There are some errors in the form.' => 'Revise los errores del formulario.'
    /* FIN GOBALO MOD */
];
