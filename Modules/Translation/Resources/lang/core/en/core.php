<?php

return [
    'modal' => [
        'title' => 'Confirmation',
        /* GOBALO MOD */
        'title-attention' => 'Attention!',
        
        'confirmation-message' => 'Are you sure you want to delete this item?',
        'confirmation-delete-bulk-message' => 'Are you going to delete the items selected. Are you sure?',
        'confirmation-children-items' => 'Delete this item imply all their direct children will be decoupled and they will be converted to parent items',
        'confirmation-children-show' => 'The next items will be converted to parent items',
        'confirmation-children-confirmation' => 'Are you sure you want to delete this items?',
        
        // Remove
        'confirmation-message-info-parent' => "If you remove the parent, this item will no longer be part of it. Are you sure?",
        
        // Trash
        'confirmation-message-info-parent-trash' => "If you delete this item, it will no longer be part of",
        'confirmation-message-trash' => 'Are you sure you want to move this item to the trash?',
        'confirmation-delete-bulk-message-trash' => 'Are you going to move the items selected to the trash. Are you sure?',
        'confirmation-children-items-trash' => 'Move this item to the trash imply all their direct children will be decoupled and they will be converted to parent items',
        'confirmation-children-show-trash' => 'The next items will be converted to parent items',
        'confirmation-children-confirmation-trash' => 'Are you sure you want to move this items to the trash?',
        /* FIN GOBALO MOD */
    ],
    'table' => [
        'created at' => 'Created at',
        'updated at' => 'Updated at',
        'deleted at' => 'Deleted at',
        'actions' => 'Actions',
        'thumbnail' => 'Thumbnail',
    ],
    'tab' => [
        'english' => 'English',
        'french' => 'French',
        'dutch' => 'Dutch',
        'italian' => 'Italian',
        'greek' => 'Ελληνικά',
        'spanish' => 'Spanish',
        'polish' => 'Polish',
        'portuguese' => 'Portuguese',
        'arabic' => 'Arabic',
        'macedonian' => 'Македонски',
        'turkish' => 'Türkçe',
        'czech' => 'Czech',
        'swedish' => 'Svenska',
        'korean' => 'Korean',
        'hungarian' => 'Magyar',
    ],
    'button' => [
        'cancel' => 'Cancel',
        'create' => 'Create',
        'update' => 'Update',
        'delete' => 'Delete',
        'reset' => 'Reset',
        'see' => 'See',
        'visualize' => 'Visualize',
        'update and back' => 'Update and go back',
        /* GOBALO MOD */
        'move-to-trash' => 'Move to trash',
        'back' => 'Back'
        /* FIN GOBALO MOD */
    ],
    'save' => 'Save',
    'confirm' => 'Confirm',
    'move' => 'Move',
    'widget' => 'Widget',
    'widgets' => 'Widgets',
    'breadcrumb' => [
        'home' => 'Home',
    ],
    'title' => [
        'translatable fields' => 'Translatable fields',
        'non translatable fields' => 'Non translatable fields',
        'create resource' => 'Create :name',
        'edit resource' => 'Edit :name',
    ],
    'general' => [
        'available keyboard shortcuts' => 'Available keyboard shortcuts on this page',
        'view website' => 'View Website',
        'complete your profile' => 'Complete your profile',
        'profile' => 'Profile',
        'sign out' => 'Sign out',
    ],
    'messages' => [
        'resource created' => ':name successfully created.',
        'resource not found' => ':name not found.',
        'resource updated' => ':name successfully updated.',
        'resource deleted' => ':name successfully deleted.',
    ],
    'back' => 'Back',
    'back to index' => 'Back to :name index',
    'permission denied' => 'Permission denied. (required permission: ":permission")',
    'list resource' => 'List :name',
    'create resource' => 'Create :name',
    'edit resource' => 'Edit :name',
    'destroy resource' => 'Delete :name',
    'error token mismatch' => 'Your session timed out, please submit the form again.',
    'error 404' => '404',
    'error 404 title' => 'Oops! This page was not found.',
    'error 404 description' => 'The page you are looking for was not found.',
    'error 500' => '500',
    'error 500 title' => 'Oops! Something went wrong',
    'error 500 description' => 'An administrator was notified.',
    'delete cancelled' => 'Delete cancelled',
    'unauthorized' => 'Unauthorized',
    'unauthorized-access' => 'You do not have the appropriate permissions to access that page.',
    'unauthenticated' => 'Niet ingelogd',
    'unauthenticated-access' => 'You need to be logged in to be able to view this page',
    'something went wrong' => 'Whoops! Something went wrong.',
    'mark as online' => 'Mark as online',
    'mark as offline' => 'Mark as offline',
    'back to backend' => 'Back to backend',
    /* GOBALO MOD */
    'Copy languages?' => 'All the available translations will be overwritten. Are you sure?',
    'Delete items selected' => 'Delete items selected',
    'Deleting...' => 'Deleting...',
    'Items deleted successfully' => 'The items selected were deleted successfully',
    'Items deleted successfully trash' => 'The items selected were moved to trash successfully',
    'restore' => 'Restore',
    'trash' => 'Trash',
    'list' => 'List',
    'Generate' => 'Generate',
    //
    'Error' => 'Error',
    'There are some errors in the form.' => 'There are some errors in the form.'
    /* FIN GOBALO MOD */
];
