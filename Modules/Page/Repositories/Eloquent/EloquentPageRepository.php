<?php

namespace Modules\Page\Repositories\Eloquent;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Events\PageIsCreating;
use Modules\Page\Events\PageIsUpdating;
use Modules\Page\Events\PageWasCreated;
use Modules\Page\Events\PageWasDeleted;
use Modules\Page\Events\PageWasUpdated;
use Modules\Page\Repositories\PageRepository;

class EloquentPageRepository extends EloquentBaseRepository implements PageRepository
{
    /**
     * @inheritdoc
     */
    public function paginate($perPage = 15)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->paginate($perPage);
        }

        return $this->model->paginate($perPage);
    }

    /**
     * Find the page set as homepage
     * @return object
     */
    public function findHomepage()
    {
        /* GOBALO MOD */
        return $this->model->whereHas('translations', function(Builder $q) {
            $q->where('is_home', 1);
            $q->where('is_active', 1);
            $q->where('locale', App::getLocale());
        })->with('translations')->first();
        /* FIN GOBALO MOD */
    }

    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }

    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {
        if (array_get($data, 'is_home') === '1') {
            $this->removeOtherHomepage();
        }

        event($event = new PageIsCreating($data));
        $page = $this->model->create($event->getAttributes());

        event(new PageWasCreated($page, $data));

        $page->setTags(array_get($data, 'tags', []));

        return $page;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        if (array_get($data, 'is_home') === '1') {
            $this->removeOtherHomepage($model->id);
        }

        event($event = new PageIsUpdating($model, $data));
        $model->update($event->getAttributes());

        event(new PageWasUpdated($model, $data));

        $model->setTags(array_get($data, 'tags', []));

        return $model;
    }

    public function destroy($page)
    {
        $page->untag();

        event(new PageWasDeleted($page));

        return $page->delete();
    }

    /* GOBALO MOD */
    /**
     * Override original findBySlug
     * {@inheritDoc}
     * @see \Modules\Core\Repositories\Eloquent\EloquentBaseRepository::findBySlug()
     */
    public function findBySlug($slug)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->hierarchizedSlug($slug);
        }
        
        return $this->model->where('slug', $slug)->first();
    }
    
    public function hierarchizedSlug($slug)
    {
        // Si fueran varios
        $slugs = explode("/", $slug);
        
        $realPage = $this->model->whereHas('translations', function (Builder $q) use ($slugs) {
            $q->where('slug', $slugs[count($slugs) - 1]);
            /* GOBALO MOD */
            $q->where('is_active', true);
            /* FIN GOBALO MOD */
        })->with('translations')->first();
        
        $page = $realPage;
        
        if($page === null)
            return null;
        
        if($page->parent_id !== 0 && count($slugs) === 1)
            return null;
        
        if($page->parent_id === 0 && count($slugs) === 1)
            return $realPage;
        
        if($page->parent_id !== 0)
        {
            for ($i = count($slugs) - 2; $i >= count($slugs) - 2; $i--) {
                if($page->parent_id)
                {
                    $page = Page::find( $page->parent_id );
                    if( $page->slug !==  $slugs[$i]) return null;
                }
                else
                {
                    return null;
                }
            }
            
            return $realPage;
        }
        
        return null;
    }
    /* FIN GOBALO MOD */
    
    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                $q->where('slug', $slug);
                $q->where('locale', $locale);
            })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }

    /**
     * Set the current page set as homepage to 0
     * @param null $pageId
     */
    private function removeOtherHomepage($pageId = null)
    {
        $homepage = $this->findHomepage();
        if ($homepage === null) {
            return;
        }
        if ($pageId === $homepage->id) {
            return;
        }

        $homepage->is_home = 0;
        $homepage->save();
    }

    /**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $pages = $this->allWithBuilder();

        /* GOBALO MOD */
        ($request->get('status') ? $pages->where('status', '=', $request->get('status')) : $pages->Where('status', '<>', 'trash'));
        /* FIN GOBALO MOD */
        
        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $pages->whereHas('translations', function ($query) use ($term) {
                $query->where('title', 'LIKE', "%{$term}%");
                $query->orWhere('slug', 'LIKE', "%{$term}%");
            })
                ->orWhere('id', $term);
        }

        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';

            if (str_contains($request->get('order_by'), '.')) {
                $fields = explode('.', $request->get('order_by'));

                $pages->with('translations')->join('page__page_translations as t', function ($join) {
                    $join->on('page__pages.id', '=', 't.page_id');
                })
                    ->where('t.locale', locale())
                    ->groupBy('page__pages.id')->orderBy("t.{$fields[1]}", $order);
            } else {
                $pages->orderBy($request->get('order_by'), $order);
            }
        }
        
        //dd($pages->toSql());

//        $pages->with('translations')->join('page__page_translations as t', function ($join) {
//            $join->on('page__pages.id', '=', 't.page_id');
//        })->where('t.locale', locale())
//            ->groupBy('page__pages.id')->orderBy("t.title", 'desc');
        return $pages->paginate($request->get('per_page', 10));
    }

    /**
     * @param Page $page
     * @return mixed
     */
    public function markAsOnlineInAllLocales(Page $page)
    {
        $data = [];
        foreach (app(LaravelLocalization::class)->getSupportedLocales() as $locale => $supportedLocale) {
            /* GOBALO MOD */
            $data[$locale] = ['status' => 'published'];
            /* FIN GOBALO MOD */
        }

        return $this->update($page, $data);
    }

    /**
     * @param Page $page
     * @return mixed
     */
    public function markAsOfflineInAllLocales(Page $page)
    {
        $data = [];
        foreach (app(LaravelLocalization::class)->getSupportedLocales() as $locale => $supportedLocale) {
            /* GOBALO MOD */
            $data[$locale] = ['status' => 'published'];
            /* FIN GOBALO MOD */
        }

        return $this->update($page, $data);
    }

    /**
     * @param array $pageIds [int]
     * @return mixed
     */
    public function markMultipleAsOnlineInAllLocales(array $pageIds)
    {
        foreach ($pageIds as $pageId) {
            $this->markAsOnlineInAllLocales($this->find($pageId));
        }
    }

    /**
     * @param array $pageIds [int]
     * @return mixed
     */
    public function markMultipleAsOfflineInAllLocales(array $pageIds)
    {
        foreach ($pageIds as $pageId) {
            $this->markAsOfflineInAllLocales($this->find($pageId));
        }
    }
}
