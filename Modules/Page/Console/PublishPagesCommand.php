<?php

namespace Modules\Page\Console;

use Illuminate\Console\Command;
use Modules\Page\Entities\Page;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;

class PublishPagesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'page:sheduled-to-published';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status sheduled to publish';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = Page::where('status', 'scheduled')->where('publish_at', '<', Carbon::now())->update(['status' => 'published']);
        $this->info($result . ' pages status updated [ scheduled to published ]');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
