<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page__pages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->boolean('is_home')->default(0);
            $table->string('template');
            /* GOBALO MOD */
            $table->string('status')->default('draft');
            $table->string('visibility')->default('private');
            $table->integer('author_id');
            $table->timestamp('publish_at')->useCurrent();
            /* FIN GOBALO MOD */
            $table->timestamps();
        });

        Schema::create('page__page_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->string('locale')->index();
            /* GOBALO MOD */
            $table->boolean('is_active')->default(false); // If language is active
            
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            /* FIN GOBALO MOD */
            $table->text('body')->nullable();;
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_description')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_type')->nullable();

            /* GOBALO MOD */
            //$table->unique(['page_id', 'locale']);
            /* FIN GOBALO MOD */
            $table->foreign('page_id')->references('id')->on('page__pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page__page_translations');
        Schema::drop('page__pages');
    }
}
