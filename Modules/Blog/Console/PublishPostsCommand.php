<?php

namespace Modules\Blog\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Modules\Blog\Entities\Post;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PublishPostsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'post:scheduled-to-published';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status scheduled to published';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = Post::where('status', 'scheduled')->where('publish_at', '<', Carbon::now())->update(['status' => 'published']);
        $this->info($result . ' post status updated [ scheduled to published ]');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
        ];
    }
    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
        ];
    }
}
