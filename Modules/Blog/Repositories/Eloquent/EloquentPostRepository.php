<?php

namespace Modules\Blog\Repositories\Eloquent;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Blog\Entities\Post;
use Modules\Blog\Events\PostIsCreating;
use Modules\Blog\Events\PostWasCreated;
use Modules\Blog\Events\PostIsUpdating;
use Modules\Blog\Events\PostWasUpdated;
use Modules\Blog\Events\PostWasDeleted;
use Modules\Blog\Repositories\PostRepository;

class EloquentPostRepository extends EloquentBaseRepository implements PostRepository
{
    /**
     * @inheritdoc
     */
    public function paginate($perPage = 15)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->with('translations')->paginate($perPage);
        }
        
        return $this->model->paginate($perPage);
    }
    
    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->model->count();
    }
    
    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data)
    {
        event($event = new PostIsCreating($data));
        $page = $this->model->create($event->getAttributes());
        
        event(new PostWasCreated($page, $data));
        
        $page->setTags(array_get($data, 'tags', []));
        
        return $page;
    }
    
    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data)
    {
        event($event = new PostIsUpdating($model, $data));
        $model->update($event->getAttributes());
        
        event(new PostWasUpdated($model, $data));
        
        $model->setTags(array_get($data, 'tags', []));
        
        return $model;
    }
    
    public function destroy($post)
    {
        $post->untag();
        
        event(new PostWasDeleted($post));
        
        return $post->delete();
    }
    
    /* GOBALO MOD */
    /**
     * Override original findBySlug
     * {@inheritDoc}
     * @see \Modules\Core\Repositories\Eloquent\EloquentBaseRepository::findBySlug()
     */
    public function findBySlug($slug)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug) {
                $q->where('slug', $slug);
                $q->where('is_active', true);
            })->with('translations')->first();
        }
        
        return $this->model->where('slug', $slug)->first();
    }
    
    /*
    public function hierarchizedSlug($slug)
    {
        // Si fueran varios
        $slugs = explode("/", $slug);
        
        $post = $this->model->whereHas('translations', function (Builder $q) use ($slugs) {
            $q->where('slug', $slugs[count($slugs) - 1]);
            $q->where('is_active', true);
        })->with('translations')->first();
        
        $page = $post;
        
        if($page === null)
            return null;
            
            if($page->parent_id !== 0 && count($slugs) === 1)
                return null;
                
                if($page->parent_id === 0 && count($slugs) === 1)
                    return $post;
                    
                    if($page->parent_id !== 0)
                    {
                        for ($i = count($slugs) - 2; $i >= count($slugs) - 2; $i--) {
                            if($page->parent_id)
                            {
                                $page = Page::find( $page->parent_id );
                                if( $page->slug !==  $slugs[$i]) return null;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        
                        return $post;
                    }
                    
                    return null;
    }*/
    /* FIN GOBALO MOD */
    
    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale)
    {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                $q->where('slug', $slug);
                $q->where('locale', $locale);
            })->with('translations')->first();
        }
        
        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }

    /**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $post = $this->allWithBuilder();
        
        ($request->get('status') ? $post->where('status', '=', $request->get('status')) : $post->Where('status', '<>', 'trash'));
        if($request->get('category_id')) {
            $catId = $request->get('category_id');
            $post->whereHas('categories', function (Builder $q) use ($catId) {
                $q->where('category_id', $catId);
            });
        }

        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $post->whereHas('translations', function ($query) use ($term) {
                $query->where('title', 'LIKE', "%{$term}%");
                $query->orWhere('slug', 'LIKE', "%{$term}%");
            })
            ->orWhere('id', $term);
        }
        
        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';
            
            if (str_contains($request->get('order_by'), '.')) {
                $fields = explode('.', $request->get('order_by'));
                
                $post->with('translations')->join('blog__post_translations as t', function ($join) {
                    $join->on('blog__post.id', '=', 't.page_id');
                })
                ->where('t.locale', locale())
                ->groupBy('blog__post.id')->orderBy("t.{$fields[1]}", $order);
            } else {
                $post->orderBy($request->get('order_by'), $order);
            }
        }
        
        return $post->paginate($request->get('per_page', 10));
    }
    
    public function getNextOf($post)
    {}

    public function getPreviousOf($post)
    {}

    public function latest($amount = 5)
    {}


}
