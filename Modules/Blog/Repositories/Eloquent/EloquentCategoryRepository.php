<?php

namespace Modules\Blog\Repositories\Eloquent;

use Modules\Blog\Repositories\CategoryRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
    
    /**
     * Paginating, ordering and searching through pages for server side index table
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function serverPaginationFilteringFor(Request $request): LengthAwarePaginator
    {
        $pages = $this->allWithBuilder();
        
        
        if ($request->get('search') !== null) {
            $term = $request->get('search');
            $pages->whereHas('translations', function ($query) use ($term) {
                $query->where('name', 'LIKE', "%{$term}%");
                $query->orWhere('slug', 'LIKE', "%{$term}%");
            })
            ->orWhere('id', $term);
        }
        
        if ($request->get('order_by') !== null && $request->get('order') !== 'null') {
            $order = $request->get('order') === 'ascending' ? 'asc' : 'desc';
            
            if (str_contains($request->get('order_by'), '.')) {
                $fields = explode('.', $request->get('order_by'));
                
                $pages->with('translations')->join('blog__category_translations as t', function ($join) {
                    $join->on('blog__categories.id', '=', 't.category_id');
                })
                ->where('t.locale', locale())
                ->groupBy('blog__categories.id')->orderBy("t.{$fields[1]}", $order);
            } else {
                $pages->orderBy($request->get('order_by'), $order);
            }
        }
        
        return $pages->paginate($request->get('per_page', 10));
    }
    
}
