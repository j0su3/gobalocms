<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',         
        'slug',
        /* GOBALO MOD */
        'is_active',
        'description'
        /* GOBALO MOD */
    ];
    protected $table = 'blog__category_translations';
    
    /* GOBALO MOD */
    protected $casts = [
        'is_active' => 'boolean'
    ];
    /* FIN GOBALO MOD*/
}
