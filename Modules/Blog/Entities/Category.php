<?php

namespace Modules\Blog\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;

    public $translatedAttributes = [
        'name', 
        'slug', 
        /* GOBALO MOD */
        'description',
        'is_active'
        /* FIN GOBALO MOD */
    ];
    protected $fillable = [
        'name',         
        'slug',
        /* GOBALO MOD */
        'description',
        'is_active',
        'parent_id',
        /* FIN GOBALO MOD */
    ];
    protected $table = 'blog__categories';

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'blog__posts_categories');
    }
    
    /* GOBALO MOD */
    
    public function parent()
    {
        return $this->belongsTo(Category::class,'parent_id');
    }
    
    public function children()
    {
        return $this->hasMany(Category::class,'parent_id')->with('children');
    }
    
    public function getCanonicalUrl()
    {
        $category = $this;
        
        $slug = '/' . $category->slug;
        
        while($category->parent_id !== 0)
        {
            $category = Category::find($category->parent_id);
            $slug = '/' . $category->slug . $slug;
        }
        
        $slug = 'blog/' . substr($slug, 1);
        
        return route('page', $slug);
    }
    
    public function getEditUrl() : string
    {
        return route('admin.blog.category.edit', $this->id);
    }
    
    public function getCategoriesHierarchy($id = -1) {
        $childrens = [];
        $children = 'children';
        
        for ($i = 0; $i < 50; $i++) {
            $childrens[$children] = function ($query) use ($id) { $query->where('id', '<>', $id); };
            $children .= '.children';
        }
        
        return Category::with($childrens)->where('parent_id', 0)->where('id', '<>', $id)->where('id', '<>', 1)->get()->toArray();
    }
    
    /* FIN GOBALO MOD */
}
