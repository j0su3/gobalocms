<?php

namespace Modules\Blog\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Modules\Blog\Presenters\PostPresenter;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Entities\File;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Tag\Contracts\TaggableInterface;
use Modules\Tag\Traits\TaggableTrait;
use Modules\User\Entities\Sentinel\User;

class Post extends Model implements TaggableInterface
{
    use Translatable, MediaRelation, PresentableTrait, NamespacedEntity, TaggableTrait;

    protected $table = 'blog__posts';
    public $translatedAttributes = [
        'post_id',
        'title',
        'slug', 
        'content',
        'is_active'
    ];
    protected $fillable = [
        'category_id', 
        'status', 
        'visibility',
        'author_id',
        'publish_at',
        'title', 
        'slug', 
        'content',
        'post_id',
        'title',
        'slug',
        'content',
        'is_active'
    ];
    protected $presenter = PostPresenter::class;
    protected $statuses = [
        'published',
        'scheduled',
        'draft'
    ];
    protected $visibilities = [
        'public',
        'private'
    ];
    protected static $entityNamespace = 'asgardcms/post';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'blog__posts_categories');
    }
    
    public function author()
    {
        return $this->belongsTo(User::class);
    }
    
    /**
     * Get the thumbnail image for the current blog post
     * @return File|string
     */
    public function getThumbnailAttribute()
    {
        $thumbnail = $this->files()->where('zone', 'thumbnail')->first();

        if ($thumbnail === null) {
            return '';
        }

        return $thumbnail;
    }

    /**
     * Check if the post is in draft
     * @param Builder $query
     * @return Builder
     */
    public function scopeDraft(Builder $query)
    {
        return $query->whereStatus(Status::DRAFT);
    }

    /**
     * Check if the post is pending review
     * @param Builder $query
     * @return Builder
     */
    public function scopePending(Builder $query)
    {
        return $query->whereStatus(Status::PENDING);
    }

    /**
     * Check if the post is published
     * @param Builder $query
     * @return Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->whereStatus(Status::PUBLISHED);
    }

    /**
     * Check if the post is unpublish
     * @param Builder $query
     * @return Builder
     */
    public function scopeUnpublished(Builder $query)
    {
        return $query->whereStatus(Status::UNPUBLISHED);
    }

    public function getCanonicalUrl()
    {
        return $this->category->getCanonicalUrl() . '/' . $this->slug;
    }
    
    /**
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        #i: Convert array to dot notation
        $config = implode('.', ['asgard.blog.config.post.relations', $method]);

        #i: Relation method resolver
        if (config()->has($config)) {
            $function = config()->get($config);

            return $function($this);
        }

        #i: No relation found, return the call to parent (Eloquent) to handle it.
        return parent::__call($method, $parameters);
    }
}
