<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Blog\Events\PostContentIsRendering;

class PostTranslation extends Model
{
    protected $table = 'blog__post_translations';
    public $timestamps = false;
    protected $fillable = [
        'post_id',
        'is_active',
        'title', 
        'slug', 
        'content'
    ];
    
    protected $casts = [
        'is_active' => 'boolean'
    ];

    public function getContentAttribute($content)
    {
        event($event = new PostContentIsRendering($content));

        return $event->getContent();
    }
}
