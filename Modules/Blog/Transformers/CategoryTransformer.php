<?php

namespace Modules\Blog\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use Modules\Blog\Entities\Post;

class CategoryTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'translations' => [
                'name' => optional($this->translate(locale()))->name,
                'slug' => optional($this->translate(locale()))->slug,
                'is_active' => optional($this->translate(locale()))->is_active
            ],
            'created_at' => $this->created_at->format('d-m-Y'),
            'urls' => [
                'public_url' => $this->getCanonicalUrl(),
                'delete_url_only_trash' => route('api.blog.category.destroyonlytrash', $this->id),
                'delete_url' => route('api.blog.category.destroy', $this->id),
                'check_children' => route('api.blog.category.checkchildren', $this->id),
            ],
            'posts'=> $this->posts()->where('status', '<>', 'trash')->get()->count(),
            // Set if "Move to trash" button is not necessary
            'trashIcon' => false
        ];
    }
}
