<?php

namespace Modules\Blog\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\App;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\CategoryTranslation;

class FullPostTransformer extends Resource
{   
    public function toArray($request)
    {
        $unCategorized = CategoryTranslation::where('category_id', 1)->where('locale', App::getLocale())->first();
        $category = CategoryTranslation::where('category_id', $this->category_id)->where('locale', App::getLocale())->first();
        
        $postData = [
            'id' => $this->id,
            'status' => $this->status,
            'visibility' => $this->visibility,
            'publish_at' => $this->publish_at,
            //'categories' => (new Category)->getCategoriesHierarchy($this->category_id == 1 ? -1 : $this->category_id),
            'categories' => $this->formatManyTranslations((new Category)->getCategoriesHierarchy(1)),
            'category_id' => $this->category_id,
            'uncategorized' => $unCategorized,
            'categories_ids' => $this->categories->pluck('id')->toArray(),
            'category' => $this->formatTranslations($this->category),
            'urls' => [
                //'public_url' => $this->getCanonicalUrl(),
                //'check_children' => route('api.blog.post.checkchildren', $this->id),
            ],
        ];

        $postData = $this->setTranslations($postData);

        return $postData;
    }

    private function setTranslations($object)
    {
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $object[$locale] = [];
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $object[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
                $object[$locale]['disabled'] = false;

                if ($translatedAttribute === 'is_active')
                    if (is_null($object[$locale][$translatedAttribute])) {
                        $object[$locale][$translatedAttribute] = false;
                        continue;
                    }
            }
        }

        return $object;
    }

    static public function formatTranslations($object)
    {
        if( isset( $object['translations'] ) ) {

            foreach ($object['translations'] as $translation) {
                $locale = $translation['locale'];
                $object[$locale] = $translation;
                unset($object[$locale]['locale']);
            }

            unset($object['translations']);

        }

        return $object;
    }

    static public function formatManyTranslations($objects)
    {
        for($i=0;$i < count($objects);$i++)
        {
            if( isset($objects[$i]['children']) )
                if( $objects[$i]['children'] )
                    $objects[$i]['children'] = FullPostTransformer::formatManyTranslations($objects[$i]['children']);

            $objects[$i] = FullPostTransformer::formatTranslations($objects[$i]);
        }

        return $objects;
    }
}
