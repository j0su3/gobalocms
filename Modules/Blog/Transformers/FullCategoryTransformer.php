<?php

namespace Modules\Blog\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class FullCategoryTransformer extends Resource
{
    public function toArray($request)
    {
        $categoryData = [
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'parent' => $this->formatTranslations($this->parent),
            'categories' => $this->formatManyTranslations($this->getCategoriesHierarchy($this->id)),
            'urls' => [
                'check_children' => route('api.blog.category.checkchildren', $this->id),
                'public_url' => $this->getCanonicalUrl(),
            ],
        ];

        $categoryData = $this->setTranslations($categoryData);

        return $categoryData;
    }

    private function setTranslations($object)
    {
        foreach (LaravelLocalization::getSupportedLocales() as $locale => $supportedLocale) {
            $object[$locale] = [];
            foreach ($this->translatedAttributes as $translatedAttribute) {
                $object[$locale][$translatedAttribute] = $this->translateOrNew($locale)->$translatedAttribute;
                $object[$locale]['disabled'] = false;

                if( $translatedAttribute === 'is_active' )
                    if( is_null( $object[$locale][$translatedAttribute] ) )
                    {
                        $object[$locale][$translatedAttribute] = false;
                        continue;
                    }

            }
            // Check for children
            $object[$locale]['disabled_by_category'] = false;
            $object[$locale]['disabled_by_post'] = false;
            if( $this->children )
            {
                foreach($this->formatManyTranslations($this->children) as $child)
                {
                    if( $child[$locale]['is_active'] )
                    {
                        $object[$locale]['disabled_by_category'] = true;
                        break;
                    }
                }
            }
            // Check for posts
            if( $this->posts )
            {
                foreach($this->formatManyTranslations($this->posts) as $post)
                {
                    //var_dump($post);
                    if( $post[$locale]['is_active'] )
                    {
                        $object[$locale]['disabled_by_post'] = true;
                        break;
                    }
                }
            }
        }

        return $object;
    }

    static public function formatTranslations($object)
    {
        if( isset( $object['translations'] ) ) {

            foreach ($object['translations'] as $translation) {
                $locale = $translation['locale'];
                $object[$locale] = $translation;
                unset($object[$locale]['locale']);
            }

            unset($object['translations']);

        }

        return $object;
    }

    static public function formatManyTranslations($objects)
    {
        for($i=0;$i < count($objects);$i++)
        {
            if( isset($objects[$i]['children']) )
                if( $objects[$i]['children'] )
                    $objects[$i]['children'] = FullCategoryTransformer::formatManyTranslations($objects[$i]['children']);

            $objects[$i] = FullCategoryTransformer::formatTranslations($objects[$i]);
        }

        return $objects;
    }
}
