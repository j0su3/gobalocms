<?php

namespace Modules\Blog\Transformers;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\App;

class PostTransformer extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'visibility' => $this->visibility,
            'category_name' => $this->category->name,
            'author' => $this->author->email,
            'publish_at' => Carbon::parse($this->publish_at)->format('d-m-Y'),
            'created_at' => $this->created_at->format('d-m-Y'),
            'translations' => [
                'title' => optional($this->translate(locale()))->title,
                'slug' => optional($this->translate(locale()))->slug,
                'is_active' => optional($this->translate(locale()))->is_active
            ],
            'category' => [
                'id' => $this->category->id,
                'name'=> $this->category->name,
                'is_active' => $this->category->is_active,
            ],
            'urls' => [
                'public_url' => $this->getCanonicalUrl(),
                'delete_url_only_trash' => route('api.blog.post.destroyonlytrash', $this->id),
                'delete_url' => route('api.blog.post.destroy', $this->id),
                'check_children' => route('api.blog.post.checkchildren', $this->id),
            ],
        ];
    }
}
