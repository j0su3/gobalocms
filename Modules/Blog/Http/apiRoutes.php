<?php

use Illuminate\Routing\Router;

/** @var Router $router */

$router->group(['prefix' => 'v1/blog', 'middleware' => 'api.token'], function (Router $router) {
    
    /* CATEGORIES */

    $router->post('categories/hierarchy', [
        'as' => 'api.blog.category.hierarchy',
        'uses' => 'V1\CategoryController@getCategoryHierarchy',
        'middleware' => 'token-can:blog.categories.edit',
    ]);
    
    /* GOBALO MOD */
    
    $router->post('categories/destroyall', [
        'as' => 'api.blog.category.destroyall',
        'uses' => 'V1\CategoryController@destroyAll',
        'middleware' => 'token-can:blog.categories.destroy',
    ]);
    
    /* FIN GOBALOMOD */ 
    
    $router->get('categories', [
        'as' => 'api.blog.category.index',
        'uses' => 'V1\CategoryController@index',
        'middleware' => 'token-can:blog.categories.index',
    ]);
    
    /* GOBALO MOD */
    
    $router->post('categories/{category}', [
        'as' => 'api.blog.category.find',
        'uses' => 'V1\CategoryController@find',
        'middleware' => 'token-can:blog.categories.edit',
    ]);
    
    $router->get('categories-server-side', [
        'as' => 'api.blog.category.indexServerSide',
        'uses' => 'V1\CategoryController@indexServerSide',
        'middleware' => 'token-can:blog.categories.index',
    ]);

    $router->delete('categories/{category}/check-children', [
        'as' => 'api.blog.category.checkchildren',
        'uses' => 'V1\CategoryController@checkChildren',
        'middleware' => 'token-can:blog.categories.destroy',
    ]);
    
    /* FIN GOBALO MOD */
    
    $router->post('categories', [
        'as' => 'api.blog.category.store',
        'uses' => 'V1\CategoryController@store',
        'middleware' => 'token-can:blog.categories.create',
    ]);
    
    /* GOBALO MOD */
    
    $router->post('categories/{category}/edit', [
        'as' => 'api.blog.category.update',
        'uses' => 'V1\CategoryController@update',
        'middleware' => 'token-can:blog.categories.edit',
    ]);
    
    $router->delete('categories/{category}/trash', [
        'as' => 'api.blog.category.destroyonlytrash',
        'uses' => 'V1\CategoryController@destroy',
        'middleware' => 'token-can:blog.categories.destroy',
    ]);
    
    /* FIN GOBALO MOD */

    $router->delete('categories/{category}', [
        'as' => 'api.blog.category.destroy',
        'uses' => 'V1\CategoryController@destroy',
        'middleware' => 'token-can:blog.categories.destroy',
    ]);
    
    /* GOBALO MOD */
    
    /* POSTS */
    
    $router->get('posts', [
        'as' => 'api.blog.post.index',
        'uses' => 'V1\PostController@index',
        'middleware' => 'token-can:blog.posts.index',
    ]);
    
    $router->get('posts-server-side', [
        'as' => 'api.blog.post.indexServerSide',
        'uses' => 'V1\PostController@indexServerSide',
        'middleware' => 'token-can:blog.posts.index',
    ]);
    
    $router->post('posts/destroyall', [
        'as' => 'api.blog.post.destroyall',
        'uses' => 'V1\PostController@destroyAll',
        'middleware' => 'token-can:blog.posts.destroy',
    ]);
    
    $router->post('posts/destroyallonlytrash', [
        'as' => 'api.blog.post.destroyallonlytrash',
        'uses' => 'V1\PostController@destroyAllOnlyTrash',
        'middleware' => 'token-can:blog.posts.destroy',
    ]);
    
    $router->delete('posts/{post}/trash', [
        'as' => 'api.blog.post.destroyonlytrash',
        'uses' => 'V1\PostController@destroyOnlyTrash',
        'middleware' => 'token-can:blog.posts.destroy',
    ]);
    
    $router->delete('posts/{post}', [
        'as' => 'api.blog.post.destroy',
        'uses' => 'V1\PostController@destroy',
        'middleware' => 'token-can:blog.posts.destroy',
    ]);
    
    
    $router->delete('posts/{post}/check-children', [
        'as' => 'api.blog.post.checkchildren',
        'uses' => 'V1\PostController@checkChildren',
        'middleware' => 'token-can:blog.posts.destroy',
    ]);
    
    $router->post('posts', [
        'as' => 'api.blog.post.store',
        'uses' => 'V1\PostController@store',
        'middleware' => 'token-can:blog.posts.create',
    ]);
    /*
    $router->post('posts/hierarchy', [
        'as' => 'api.blog.post.hierarchy',
        'uses' =>  'V1\PostController@getPageHierarchy',
        'middleware' => 'token-can:blog.posts.edit',
    ]);
    */
    $router->post('posts/{post}', [
        'as' => 'api.blog.post.find',
        'uses' => 'V1\PostController@find',
        'middleware' => 'token-can:blog.posts.edit',
    ]);
    
    $router->post('posts/{post}/edit', [
        'as' => 'api.blog.post.update',
        'uses' => 'V1\PostController@update',
        'middleware' => 'token-can:blog.posts.edit',
    ]);
    
    $router->post('posts/{id}/clone', [
        'as' => 'api.blog.post.clone',
        'uses' => 'V1\PostController@clone',
        'middleware' => 'can:blog.posts.edit',
    ]);
    
    $router->post('posts/categories/hierarchy', [
        'as' => 'api.blog.posts.categories.hierarchy',
        'uses' => 'V1\PostController@getCategoryHierarchy',
        'middleware' => 'token-can:blog.posts.edit',
    ]);
    /* FIN GOBALO MOD */
    
});

