<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\Post;
use Modules\Blog\Repositories\PostRepository;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Blog\Repositories\CategoryRepository;

class PublicController extends BasePublicController
{
    /**
     * @var PostRepository
     */
    private $post;
    private $category;
    private $published = 'published';
    private $private = 'private';
    
    public function __construct(PostRepository $post, CategoryRepository $category, Application $app)
    {
        parent::__construct();
        $this->post = $post;
        $this->category = $category;
        $this->app = $app;
    }

    public function index()
    {
        $posts = $this->post->allTranslatedIn(App::getLocale())->where('status', 'published');

        return view('blog.index', compact('posts'));
    }
    
    public function show($slug)
    {
        
        $slugs = explode('/', $slug);
        
        $mainSlug = $slugs[count($slugs) - 1];
        
        // Search for post
        $post = $this->post->findBySlug($mainSlug);
        
        // If post doesn't exist
        if(!$post)
        {
            
            $category = $this->category->findBySlug($mainSlug);

            // If category doesn't exist
            if(!$category) $this->app->abort('404');
            
            if(URL::current() !== $category->getCanonicalUrl())
                $this->app->abort('404');
            

            $posts = null;

            // If user logged
            if($this->auth->user())
            {
                // If user is admin there is no exception
                if($this->auth->user()->hasRoleName('admin'))
                {
                    // Admin can view all posts [ Except inactives translates :) Slug unique ]
                    $posts = $category->posts;
                }
                else
                {
                    $posts = $category->posts->where('status', 'published');
                }
            }
            else
            {
                $posts = $category->posts->where('status', 'published')->where('visibility', 'public');
            }
            
            return view('blog.index', compact('posts'));
            
        }
        
        // If post exists
        if(URL::current() !== $post->getCanonicalUrl())
            $this->app->abort('404');
        /* GOBALO MOD */
        
        $this->viewPostPermission($post);
        
        /* FIN GOBALO MOD */
        
        return view('blog.show', compact('post'));
    }
    
    /* GOBALO MOD */
    
    /**
    * Verify if user ( or no logged user ) can view the post
    * @param unknown $page
    */
    private function viewPostPermission($post)
    {
        // If post available
        if($post)
        {
            // If user logged
            if($this->auth->user())
            {
                // If user is admin there is no exception
                if($this->auth->user()->hasRoleName('admin'))
                {
                    // Admin can view all posts [ Except inactives translates :) Slug unique ]
                }
                else
                {
                    $this->throw404IfNotFound($post);
                }
            }
            else
            {
                $this->throw404IfNotFound($post);
            }
        }
        else
        {
            $this->throw404IfNotFound($post);
        }
        
    }
    
    /**
     * Throw a 404 error page if the given post is not found or draft or inactive translation
     * @param $page
     */
    private function throw404IfNotFound($post)
    {
        if($this->auth->user()){
            if (null === $post
                || $post->status !== $this->published || $post->is_active === false
                ) {
                    $this->app->abort('404');
                }
        }
        else {
            if (null === $post
                || $post->status !== $this->published || $post->is_active === false || $post->visibility === $this->private
                ) {
                    $this->app->abort('404');
                }
        }
    }   
    
    /* FIN GOBALO MOD */
}
