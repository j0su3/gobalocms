<?php

namespace Modules\Blog\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\CategoryTranslation;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\PostTranslation;
use Modules\Blog\Http\Requests\CreatePostRequest;
use Modules\Blog\Http\Requests\UpdatePostRequest;
use Modules\Blog\Repositories\PostRepository;
use Modules\Blog\Transformers\PostTransformer;
use Modules\Blog\Transformers\FullPostTransformer;
use Modules\Blog\Transformers\FullCategoryTransformer;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Modules\Page\Entities\PageTranslation;

class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    private $post;
    /* GOBALO MOD */
    private $defaultLocale;
    private $slugsDB;
    /* FIN GOBALO MOD*/
    
    public function __construct(PostRepository $post)
    {
        $this->post = $post;
        /* GOBALO MOD */
        $this->defaultLocale = env('LOCALE');
        /* FIN GOBALO MOD */
    }
    
    public function index()
    {
        return PostTransformer::collection($this->post->all());
    }
    
    public function indexServerSide(Request $request)
    {
        return PostTransformer::collection($this->post->serverPaginationFilteringFor($request));
    }
    
    public function store(CreatePostRequest $request)
    {
        $categories_ids = [];
        /* GOBALO MOD */
        // Set is_active default locale always true
        $request->merge($this->modifiyRequest($request));
        
        $this->validateSlug($request);
        $request->merge($this->setAuthor($request));
        /* FIN GOBALO MOD */
        
        $post = $this->post->create($request->all());
        
        if($request->categories_ids !== null)
            $categories_ids = array_unique(array_merge($request->categories_ids, [$request->category_id]));
        else
            $categories_ids = [1];

        $post->categories()->sync($categories_ids);
        
        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.post created'),
        ]);
    }
    
    public function find(Post $post)
    {
        return new FullPostTransformer($post);
    }
    
    public function update(Post $post, UpdatePostRequest $request)
    {
        /* GOBALO MOD */
        // Set is_active default locale always true
        $request->merge($this->modifiyRequest($request));
        
        $this->validateSlug($request, true);
        $request->merge($this->setAuthor($request));
        /* FIN GOBALO MOD */
        
        if($request->categories_ids !== null)
            $categories_ids = array_unique(array_merge($request->categories_ids, [$request->category_id]));
        else
            $categories_ids = [1];
        
        $post->categories()->sync($categories_ids);
        
        $this->post->update($post, $request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.post updated'),
        ]);
    }
    
    /* GOBALO MOD */
    
    /**
     * Slug validation ( Request + Database )
     * @param unknown $request
     * @param boolean $update
     */
    private function validateSlug($request, $update = false)
    {
        $this->placeSlugsDB();
        
        $slugs = [];
        $messages = [];
        
        // Save request slugs
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
                if( $request[$key]['is_active'] )
                    if( isset($request[$key]['slug']) )
                        if( $request[$key]['slug'] )
                            $slugs[$key] = $request[$key]['slug'];
        }
        
        // Validate each slug against every one else ( except itself )
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
                if( $request[$key]['is_active'] )
                    if( isset($request[$key]['slug']) )
                        if( $request[$key]['slug'] )
                            $messages = $this->addMessages($request, $slugs, $key, $messages, $update);
        }
        
        if( $messages )
        {
            throw ValidationException::withMessages($messages);
        }
    }
    
    private function addMessages($request, $slugs, $key, $messages, $update = false)
    {
        $slugsTemp = $slugs ;
        $slugsDB = $this->slugsDB;
        
        // Remove itself from array
        unset($slugsTemp[$key]);
        
        // Remove current post slugs for validation in update
        if($update) $slugsDB = array_diff($slugsDB, PostTranslation::where('post_id', $request->id)->where('is_active', true)->get()->pluck('slug')->toArray());
        
        // Validate slugs
        if(in_array($request[$key]['slug'], array_merge($slugsTemp, $slugsDB))){
            $messages[$key . '.slug'] = $request->translationMessages()['slug.unique'];
        }
        
        return $messages;
    }
    
    /**
     * Fix is_active null from request
     * @param unknown $inputs
     * @param unknown $locales
     */
    private function fixIsActive($inputs, $locales)
    {
        
        foreach ($locales as $key => $value){
            // Always true for default language
            if($key === $this->defaultLocale)
            {
                $inputs[$this->defaultLocale]['is_active'] = true;
                continue;
            }
            
            if(isset($inputs[$key]['is_active']))
            {
                if($inputs[$key]['is_active'] ===  null) $inputs[$key]['is_active'] = false;
            }
            else
            {
                $inputs[$key]['is_active'] = false;
            }
        }
        
        return $inputs;
    }
    
    /*
     * Set status published if status is "scheduled" and publish_date is less than now
     */
    private function verifyPublished($request)
    {
        if(!isset($request['publish_at'])) $request['publish_at'] = Carbon::now();
        switch ($request['status'])
        {
            case "published":
                if(strtotime($request['publish_at']) > strtotime($request['current_time']))
                    $request['status'] = 'scheduled';
                    break;
            case "scheduled":
                if(strtotime($request['publish_at']) <= strtotime($request['current_time']))
                    $request['status'] = 'published';
                    break;
        }
        
        return $request;
    }
    
    private function setAuthor($request)
    {
        $inputs = $request->all();
        $inputs['author_id'] = Auth::id();
        
        return $inputs;
    }
    
    /**
     * Set is_active for defautl locale always true and convert is_active from null to false
     * @param unknown $request
     * @return boolean
     */
    private function modifiyRequest($request)
    {
        $inputs = $request->all();
        
        $inputs = $this->fixIsActive($inputs, $request->requiredLocales());
        $inputs = $this->verifyPublished($inputs);
        // $this->verifyTrash($request);
        
        return $inputs;
    }

    /**
     * Place all the slugs actives in $slugsDB variable
     */
    private function placeSlugsDB()
    {
        $this->slugsDB = [];
        foreach (array_unique(PageTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }

        foreach (array_unique(CategoryTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }


        foreach (array_unique(PostTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }
    }
    
    public function destroy(Post $post)
    {
        $post->categories()->sync([]);
        $this->post->destroy($post);
        
        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.post deleted'),
        ]);
    }
    
    public function destroyOnlyTrash(Post $post)
    {
        Post::where('id', $post->id)->update(['status' => 'trash', 'category_id' => 1 ]);
        $post->categories()->sync([]);

        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.post deleted trash'),
        ]);
    }
    
    
    public function checkChildren(Post $post)
    {
        
        $data = [];
        $message = "";
        
        /* Categoría principal */
        
        if($post->category_id)
            if($post->category_id !== 1)
            {
                $message .= 'Este post está vinculado a alguna categoría. <br> <br>';
                $message .= trans('core::core.modal.confirmation-message-trash');
            }
        
        /* */

        /* Categorias asociadas */
        /*
        if($post->children->count())
        {
            $pages = $post->children;
            $data = $post->children->pluck('title')->toArray();
            $message .= trans('core::core.modal.confirmation-children-items-trash') . '.<br><br>';
            $message .= trans('core::core.modal.confirmation-children-show-trash') . ':<br>';
            $message .= "<ul>";
            foreach ($pages as $pageAux)
            {
                $pageTitle = ( $pageAux->is_active ? $pageAux->title : $pageAux->id . ' - ' . trans('translation::translations.Inactive translation')  );
                $message .= "<li><b>". $pageTitle ."</b></li>";
            }
            $message .= "</ul><br>";
        }
        */
        /* */
        /*
        if( $post->parent_id || $post->children->count())
        {
            $message .= trans('core::core.modal.confirmation-message-trash');
        }*/

        return response()->json([
            'errors' => false,
            'message' => $message,
            'data' => $data
        ]);
    }
    
    public function destroyAll(Request $request)
    {
        Post::destroy($request->get('ids'));
        
        return response()->json([
            'errors' => false,
            'message' => trans('core::core.Items deleted successfully'),
        ]);
    }
    
    public function destroyAllOnlyTrash(Request $request)
    {
        $query = Post::query();
        
        foreach ($request->get('ids') as $id) {
            $query->orWhere('id', $id);
            Post::find($id)->categories()->sync([]);
        }
        
        $query->update(['status' => 'trash', 'category_id' => 1]);
        
        return response()->json([
            'errors' => false,
            'message' => trans('core::core.Items deleted successfully trash'),
        ]);
    }
    
    /**
     * Clone a post
     * @param unknown $id
     */
    public function clone($id)
    {
        $this->placeSlugsDB();
        
        $post = $this->post->find($id)->toArray();
        
        // null variables
        $post['id'] = null;
        $post['post_id'] = null;
        $post['category_id'] = 1;
        $post['status'] = 'draft';
        $post['visibility'] = 'private';
        $post['author_id'] = Auth::id();
        $post['publish_at'] = Carbon::now();
        
        // Add translations
        foreach($post['translations'] as $translation)
        {
            // Slug unique
            while(in_array($translation['slug'], $this->slugsDB))
            {
                $translation['slug'] .= '-' . trans('translation::translations.copy');
            }
            $post[$translation['locale']] = $translation;
        }
        
        $this->post->create($post);
    }
    /*
    public function verifyTrash($request)
    {
        if($request->status === 'trash')
        {
            Post::where('parent_id', $request->id)->update(['parent_id' => 0]);
            Post::where('id', $request->id)->update(['parent_id' => 0]);
        }
    }*/
    /* FIN GOBALO MOD */
    
    public function getCategoryHierarchy()
    {
        return response()->json([
            'categories' => FullCategoryTransformer::formatManyTranslations((new Category())->getCategoriesHierarchy(1)),
            'uncategorized' => FullCategoryTransformer::formatTranslations(Category::find(1))
        ]);
    }
}
