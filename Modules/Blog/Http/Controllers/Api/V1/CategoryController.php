<?php

namespace Modules\Blog\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\CategoryTranslation;
use Modules\Blog\Entities\Post;
use Modules\Blog\Entities\PostTranslation;
use Modules\Blog\Http\Requests\CreateCategoryRequest;
use Modules\Blog\Repositories\CategoryRepository;
use Modules\Page\Entities\PageTranslation;
use Illuminate\Validation\ValidationException;
use Modules\Blog\Transformers\FullCategoryTransformer;
use Modules\Blog\Http\Requests\UpdateCategoryRequest;
use Modules\Blog\Transformers\CategoryTransformer;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $category;
    /* GOBALO MOD */
    private $defaultLocale;
    private $slugsDB;
    /* FIN GOBALO MOD */

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
        /* GOBALO MOD */
        $this->defaultLocale = env('LOCALE');
        /* FIN GOBALO MOD */
    }

    public function index()
    {
        $categories = $this->category->all();

        return response()->json([
            'errors' => false,
            'count' => $categories->count(),
            'data' => $categories,
        ]);
    }
    
    public function indexServerSide(Request $request)
    {
        return CategoryTransformer::collection($this->category->serverPaginationFilteringFor($request));
    }

    /* GOBALO MOD */
    
    public function store(CreateCategoryRequest $request)
    {
        $request->merge($this->modifiyRequest($request));
        $this->validateSlug($request);
        
        $this->category->create($request->all());
        
        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.category created')
        ]);
    }

    public function find(Category $category)
    {
        return new FullCategoryTransformer($category);
    }
    
    public function update(Category $category, UpdateCategoryRequest $request)
    {
        $request->merge($this->modifiyRequest($request));
        $this->validateSlug($request, true);

        $this->category->update($category, $request->all());

        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.category updated'),
        ]);
    }
    
    public function destroy(Category $category)
    {
        if($category->id === 1) die('Impossible to remove Uncategorized category');
        
        Category::where('parent_id', $category->id)->update(['parent_id' => 0]);
        Post::where('category_id', $category->id)->update(['category_id' => 1]);

        $category->posts()->sync([]);

        $this->category->destroy($category);
        
        return response()->json([
            'errors' => false,
            'message' => trans('blog::messages.category deleted'),
        ]);
    }
    
    public function destroyAll(Request $request)
    {
        $ids = $request->get('ids');
        
        // Search for category 1 / Impossible to remove Uncategorized category
        $category1 = array_search(1 , $request->get('ids'));
        
        if($category1 !== false)
            unset($ids[$category1]);

        $catQuery = Category::query();
        $postQuery = Post::query();

        foreach ($ids as $id) {
            $cat = Category::find($id);

            if( $cat->posts()->get()->count() > 0 ){
                unset($ids[array_search($id, $ids)]);
                continue;
            }

            $catQuery->orWhere('parent_id', $id);
            $postQuery->orWhere('category_id', $id);

            $cat->posts()->sync([]);
        }

        $catQuery->update(['parent_id' => 0]);
        $postQuery->update(['category_id' => 1]);

        Category::destroy($ids);

        return response()->json([
            'errors' => false,
            'message' => trans('core::core.Items deleted successfully'),
        ]);
    }
    
    /**
     * Slug validation ( Request + Database )
     * @param unknown $request
     * @param boolean $update
     */
    private function validateSlug($request, $update = false)
    {
        $this->placeSlugsDB();
        
        $slugs = [];
        $messages = [];
        
        // Save request slugs
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
            {
                // If parent is_active = false this.page.is_active = false
                if( $request['parent'] )
                    if( !$request['parent'][$key]['is_active'] ) {
                        $inputs = $request->all();
                        $inputs[$key]['is_active'] = false;
                        $request->merge($inputs);
                    }
                //
                if ($request[$key]['is_active'])
                    if (isset($request[$key]['slug']))
                        if ($request[$key]['slug'])
                            $slugs[$key] = $request[$key]['slug'];
            }
        }
        
        // Validate each slug against every one else ( except itself )
        foreach ($request->requiredLocales() as $key => $value) {
            if( isset($request[$key]['is_active']) )
                if( $request[$key]['is_active'] )
                    if( isset($request[$key]['slug']) )
                        if( $request[$key]['slug'] )
                            $messages = $this->addMessages($request, $slugs, $key, $messages, $update);
        }
        
        if( $messages )
        {
            throw ValidationException::withMessages($messages);
        }
    }
    
    private function addMessages($request, $slugs, $key, $messages, $update = false)
    {
        $slugsTemp = $slugs ;
        $slugsDB = $this->slugsDB;
        
        // Remove itself from array
        unset($slugsTemp[$key]);
        
        // Remove current category slugs for validation in update
        if($update) $slugsDB = array_diff($slugsDB, CategoryTranslation::where('category_id', $request->id)->where('is_active', true)->get()->pluck('slug')->toArray());
        
        // Validate slugs
        if(in_array($request[$key]['slug'], array_merge($slugsTemp, $slugsDB))){
            $messages[$key . '.slug'] = $request->translationMessages()['slug.unique'];
        }
        
        return $messages;
    }

    /**
     * Place all the slugs actives in $slugsDB variable
     */
    private function placeSlugsDB()
    {
        $this->slugsDB = [];
        foreach (array_unique(PageTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }

        foreach (array_unique(CategoryTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }


        foreach (array_unique(PostTranslation::where('is_active', true)->get()->pluck('slug')->toArray()) as $slug){
            $this->slugsDB[] = $slug;
        }
    }
    
    /**
     * Set is_active for defautl locale always true and convert is_active from null to false
     * @param unknown $request
     * @return boolean
     */
    private function modifiyRequest($request)
    {
        $inputs = $request->all();
        $inputs = $this->fixIsActive($inputs, $request->requiredLocales());
        
        return $inputs;
    }
    
    /**
     * Fix is_active null from request
     * @param unknown $inputs
     * @param unknown $locales
     */
    private function fixIsActive($inputs, $locales)
    {
        foreach ($locales as $key => $value){
            // Always true for default language
            if($key === $this->defaultLocale)
            {
                $inputs[$this->defaultLocale]['is_active'] = true;
                continue;
            }
            
            if(isset($inputs[$key]['is_active']))
            {
                if($inputs[$key]['is_active'] ===  null) $inputs[$key]['is_active'] = false;
            }
            else
            {
                $inputs[$key]['is_active'] = false;
            }
        }
        
        return $inputs;
    }
   
    public function checkChildren(Category $category)
    {
        $data = [];
        $message = "";
        
        if($category->parent_id)
        {
            $categoryName = ( $category->parent->name ? $category->parent->name  : $category->parent->id . ' - ' . trans('translation::translations.Inactive translation')  );
            $message .= trans('blog::categories.parent category assigned') . ': <br> <b>' . $categoryName .'</b>. <br><br>';
        }
        
        if($category->children->count())
        {
            $data = $category->children->pluck('id', 'name')->toArray();
            $message .= trans('blog::categories.children categories assigned') . ': <br>';
            $message .= "<ul>";
            foreach ($data as $name => $id)
            {
                $categoryName = ( $name ? $name : $id . ' - ' . trans('translation::translations.Inactive translation')  );
                $message .= "<li><b>". $categoryName ."</b></li>";
            }
            $message .= "</ul><br>";
        }

        if($category->posts->count())
        {
            $message .= trans('blog::categories.Posts associated') . '<br><br>';
        }

        if( $category->parent_id || $category->children->count() || $category->posts->count())
        {
            $message .= trans('core::core.modal.confirmation-message');
        }
        
        return response()->json([
            'errors' => false,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function getCategoryHierarchy()
    {
        return FullCategoryTransformer::formatManyTranslations((new Category())->getCategoriesHierarchy());
    }
    
    /* FIN GOBALO MDO */
}
