<?php

namespace Modules\Blog\Http\Requests;

use Illuminate\Validation\Rule;
use Modules\Core\Internationalisation\BaseFormRequest;

class CreatePostRequest extends BaseFormRequest
{
    protected $translationsAttributesKey = 'blog::posts.validation.attributes';
    
    public function rules()
    {
        $rules = [];
        $defaultLocale = env('LOCALE');
        
        foreach ($this->requiredLocales() as $localeKey => $locale) {
            if( $localeKey === $defaultLocale )
            {
                $rules = $this->addRules($rules, $localeKey, true);
            }
            else if( isset($this->request->get($localeKey)['is_active']) )
            {
                if( $this->request->get($localeKey)['is_active'] )
                {
                    $rules = $this->addRules($rules, $localeKey);
                }
            }
        }
        
        return $rules;
    }
    
    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }
    
    public function messages()
    {
        return [
            'title.required' => trans('blog::messages.title is required'),
        ];
    }

    public function translationMessages()
    {
        return [
            'title.required' => trans('blog::messages.title is required'),
            //
            'slug.required' => trans('blog::messages.slug is required'),
            'slug.regex' => trans('blog::messages.slug regex'),
            'slug.unique' => trans('blog::messages.slug is unique'),
            'slug.not_in' => trans('blog::messages.slug is reserved'),
        ];
    }
    
    public function addRules($rules, $localeKey, $defaultLocale = false)
    {
        $rules[$localeKey . '.title'] = 'required';
        $rules[$localeKey . '.slug'] = [ 'required', 'regex:/^[a-zA-Z0-9-_]+$/u' ];
        if( $defaultLocale ) array_push($rules[$localeKey . '.slug'] ,Rule::notIn(array_keys(config('asgard.core.available-locales'))));
        
        return $rules;
    }
}
