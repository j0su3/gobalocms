<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog__category_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            /* GOBALO MOD */
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->integer('category_id')->unsigned();
            $table->string('locale')->index();
            $table->boolean('is_active')->default(false);
            //$table->unique(['category_id', 'locale']);
            /* FIN GOBALO MOD */
            $table->foreign('category_id')->references('id')->on('blog__categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog__category_translations');
    }
}
