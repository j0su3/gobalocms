<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog__categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            /* GOBALO MOD */
            $table->integer('parent_id')->default(0);
            /* FIN GOBALO MOD */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog__categories');
    }
}
