<?php

namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Blog\Repositories\CategoryRepository;

class BlogDatabaseSeeder extends Seeder
{
    /**
     * @var CategoryRepository
     */
    private $category;

    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }
    public function run()
    {
        Model::unguard();
        
        $data = [
            'parent_id' => 0,
            // Translations
            'es' => [
                'name' => 'Sin categoria',
                'slug' => 'sin-categoria',
                'description' => '',
                'is_active' => 1
            ],
            'en' => [
                'name' => 'Uncategorized',
                'slug' => 'uncategorized',
                'description' => '',
                'is_active' => 0
            ],
        ];
        
        $this->category->create($data);
        
    }
}
